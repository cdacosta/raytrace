#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2013 Carlos Alberto da Costa

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division
from math import *

def rk4(f, h, pos, *args):
    '''
    Steps one RK4 iteration of size h on the variable in position pos on a
    function f that has *args.
    '''
    k1 = f(*args)

    inc = 0.5*h*k1
    k2 = f(*(args[i] if i!=pos else args[i]+inc for i in range(0, len(args))))

    inc = 0.5*h*k2
    k3 = f(*(args[i] if i!=pos else args[i]+inc for i in range(0, len(args))))

    inc = h*k3
    k4 = f(*(args[i] if i!=pos else args[i]+inc for i in range(0, len(args))))

    return args[pos] + h*(k1+2*k2+2*k3+k4)/6.0


def v(x, y, z):
    ''' Returns the velocity at point X. '''
    return 1.5 + 0.1*(x+y+z) + 0.5*sin(4*y)

def dvdx(x, y, z):
    ''' Returns the ∂v/∂x at point X. '''
    return 0.1

def dvdy(x, y, z):
    ''' Returns the ∂v/∂y at point X. '''
    return 0.1 + 0.5*cos(4*y)*4

def dvdz(x, y, z):
    ''' Returns the ∂v/∂z at point X. '''
    return 0.1

def d2vdx2(x, y, z):
    ''' Returns the ∂²v/∂x² at point X. '''
    return 0

def d2vdy2(x, y, z):
    ''' Returns the ∂²v/∂y² at point X. '''
    return -8*sin(4*y)

def d2vdz2(x, y, z):
    ''' Returns the ∂²v/∂x² at point X. '''
    return 0

def d2vdydx(x, y, z):
    ''' Returns the ∂²v/∂y∂x at point X. '''
    return 0

def d2vdzdx(x, y, z):
    ''' Returns the ∂²v/∂z∂x at point X. '''
    return 0

def d2vdzdy(x, y, z):
    ''' Returns the ∂²v/∂z∂y at point X. '''
    return 0

def d2vdxdy(x, y, z):
    ''' Returns the ∂²v/∂x∂y at point X. '''
    return d2vdydx(x, y, z)

def d2vdxdz(x, y, z):
    ''' Returns the ∂²v/∂x∂z at point X. '''
    return d2vdzdx(x, y, z)

def d2vdydz(x, y, z):
    ''' Returns the ∂²v/∂y∂z at point X. '''
    return d2vdzdy(x, y, z)

def fx(t, x, y, z, px, py, pz):
    ''' Returns fx such that dx/dt = fx(X, P).'''
    return px*v(x, y, z)**2

def fy(t, x, y, z, px, py, pz):
    ''' Returns fy such that dx/dt = fx(X, P).'''
    return py*v(x, x, z)**2

def fz(t, x, y, z, px, py, pz):
    ''' Returns fz such that dz/dt = fz(X, P).'''
    return pz*v(x, y, z)**2

def fpx(t, x, y, z, px, py, pz):
    ''' Returns fpx such that dpx/dt = fpx(X, P).'''
    return -dvdx(x, y, z)/v(x, y, z)

def fpy(t, x, y, z, px, py, pz):
    ''' Returns fpx such that dpx/dt = fpx(X, P).'''
    return -dvdy(x, y, z)/v(x, y, z)

def fpz(t, x, y, z, px, py, pz):
    ''' Returns fpz such that dpz/dt = fpz(X, P).'''
    return -dvdz(x, y, z)/v(x, y, z)

def fe11(t, x, y, z, px, py, pz, e11, e12, e13):
    ''' Returns fe11 such that de11/dt = fe11(e1, P).'''
    # Corresponds to e₁ · ∇v
    e1dv = e11*dvdx(x, y, z) + e12*dvdy(x, y, z) + e13*dvdz(x, y, z)
    return e1dv*px

def fe12(t, x, y, z, px, py, pz, e11, e12, e13):
    ''' Returns fe11 such that de12/dt = fe12(e1, P).'''
    # Corresponds to e₁ · ∇v
    e1dv = e11*dvdx(x, y, z) + e12*dvdy(x, y, z) + e13*dvdz(x, y, z)
    return e1dv*py

def fe13(t, x, y, z, px, py, pz, e11, e12, e13):
    ''' Returns fe11 such that de13/dt = fe13(e1, P).'''
    # Corresponds to e₁ · ∇v
    e1dv = e11*dvdx(x, y, z) + e12*dvdy(x, y, z) + e13*dvdz(x, y, z)
    return e1dv*pz

def fP11(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 11 of dP/dt = f(P) = SP. '''
    return P31*v(x, y, z)**2

def fP12(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 12 of dP/dt = f(P) = SP. '''
    return P32*v(x, y, z)**2

def fP13(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 13 of dP/dt = f(P) = SP. '''
    return P33*v(x, y, z)**2

def fP14(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 14 of dP/dt = f(P) = SP. '''
    return P34*v(x, y, z)**2

def fP21(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 21 of dP/dt = f(P) = SP. '''
    return P41*v(x, y, z)**2

def fP22(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 22 of dP/dt = f(P) = SP. '''
    return P42*v(x, y, z)**2

def fP23(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 23 of dP/dt = f(P) = SP. '''
    return P43*v(x, y, z)**2

def fP24(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 24 of dP/dt = f(P) = SP. '''
    return P44*v(x, y, z)**2

def fP31(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 31 of dP/dt = f(P) = SP. '''
    fp  = -V11(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P11
    fp -= -V12(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P21
    return fp/v(x, y, z)

def fP32(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 32 of dP/dt = f(P) = SP. '''
    fp  = -V11(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P12
    fp -= -V12(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P22
    return fp/v(x, y, z)

def fP33(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 33 of dP/dt = f(P) = SP. '''
    fp  = -V11(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P13
    fp -= -V12(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P23
    return fp/v(x, y, z)

def fP34(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 34 of dP/dt = f(P) = SP. '''
    fp  = -V11(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P14
    fp -= -V12(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P24
    return fp/v(x, y, z)

def fP41(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 41 of dP/dt = f(P) = SP. '''
    return 0
    fp  = -V21(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P11
    fp -= -V22(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P21
    return fp/v(x, y, z)

def fP42(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 42 of dP/dt = f(P) = SP. '''
    fp  = -V21(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P12
    fp -= -V22(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P22
    return fp/v(x, y, z)

def fP43(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 43 of dP/dt = f(P) = SP. '''
    fp  = -V21(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P13
    fp -= -V22(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P23
    return fp/v(x, y, z)

def fP44(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33,
         P11, P12, P13, P14, P21, P22, P23, P24, P31, P32, P33, P34,
         P41, P42, P43, P44):
    ''' Returns element 44 of dP/dt = f(P) = SP. '''
    fp  = -V21(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P14
    fp -= -V22(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23,
               e31, e32, e33)*P24
    return fp/v(x, y, z)

def V11(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33):
    ''' Returns element 11 of V. '''
    V =e11*e11*d2vdx2(x,y,z)  + e11*e12*d2vdydx(x,y,z) + e11*e13*d2vdzdx(x,y,z)
    V+=e12*e11*d2vdxdy(x,y,z) + e12*e12*d2vdy2(x,y,z)  + e12*e13*d2vdzdy(x,y,z)
    V+=e13*e11*d2vdxdz(x,y,z) + e13*e12*d2vdydz(x,y,z) + e13*e13*d2vdz2(x,y,z)
    return V

def V12(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33):
    ''' Returns element 12 of V. '''
    V =e11*e21*d2vdx2(x,y,z)  + e11*e22*d2vdydx(x,y,z) + e11*e23*d2vdzdx(x,y,z)
    V+=e12*e21*d2vdxdy(x,y,z) + e12*e22*d2vdy2(x,y,z)  + e12*e23*d2vdzdy(x,y,z)
    V+=e13*e21*d2vdxdz(x,y,z) + e13*e22*d2vdydz(x,y,z) + e13*e23*d2vdz2(x,y,z)
    return V

def V21(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33):
    ''' Returns element 21 of V. '''
    V =e21*e11*d2vdx2(x,y,z)  + e21*e12*d2vdydx(x,y,z) + e21*e13*d2vdzdx(x,y,z)
    V+=e22*e11*d2vdxdy(x,y,z) + e22*e12*d2vdy2(x,y,z)  + e22*e13*d2vdzdy(x,y,z)
    V+=e23*e11*d2vdxdz(x,y,z) + e23*e12*d2vdydz(x,y,z) + e23*e13*d2vdz2(x,y,z)
    return V

def V22(t, x, y, z, px, py, pz, e11, e12, e13, e21, e22, e23, e31, e32, e33):
    ''' Returns element 22 of V. '''
    V =e21*e21*d2vdx2(x,y,z)  + e21*e22*d2vdydx(x,y,z) + e21*e23*d2vdzdx(x,y,z)
    V+=e22*e21*d2vdxdy(x,y,z) + e22*e22*d2vdy2(x,y,z)  + e22*e23*d2vdzdy(x,y,z)
    V+=e23*e21*d2vdxdz(x,y,z) + e23*e22*d2vdydz(x,y,z) + e23*e23*d2vdz2(x,y,z)
    return V
