Raytrace
========

Raytrace is a simple ray tracing program written in Python.
To run it, execute:

    python raytrace.py

It uses ``matplotlib`` for graphing, but otherwise only includes the ``math`` module from the Python Standard Library.

Samples
=======

# 3D plot
![Rays in 3D](https://dl.dropbox.com/s/wszpmxwa1lnvpgf/3d.png)

# 2D plots of projections
![Projections of rays](https://dl.dropbox.com/s/4vsx71vfat51p4b/projections.png)

