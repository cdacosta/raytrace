#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2013 Carlos Alberto da Costa

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division

from raytracefun import *

import matplotlib.pyplot as plt
from math import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
            FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
            self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

# Parameters
rays = 16
h = 0.0005
N = 2000

# Auxiliary
zeros = [ [0] for a in range(0, rays) ]
ones  = [ [1] for a in range(0, rays) ]


# POINT SOURCE
# Initial conditions in global Cartesian coordinates
#t =  [0]
#x =  [ [0] for ix in range(0, rays) ]
#y =  [ [0] for iy in range(0, rays) ]
#z =  [ [0] for iz in range(0, rays) ]
#px = [ [cos(a*pi/180)/sqrt(2)] for a in range(60, 120, int((120-60)/rays)) ]
#py = [ [1.0/sqrt(2)] for iy in range(0, rays) ]
#pz = [ [sin(a*pi/180)/sqrt(2)] for a in range(60, 120, int((120-60)/rays)) ]
## Initial conditions in ray-centered coordinates basis vectors
## e1 must be perpendicular to p, we choose (py, -px, 0) normalized
#e11 =  [ [ py[r][0]/sqrt(px[r][0]**2 + py[r][0]**2) ] for r in range(0, rays) ]
#e12 =  [ [-px[r][0]/sqrt(px[r][0]**2 + py[r][0]**2) ] for r in range(0, rays) ]
#e13 =  [ [0] for ie in range(0, rays) ]

# IMAGE RAYS
# Initial conditions in global Cartesian coordinates
t =  [0]
x =  [ [0], [1], [2], [3], [0], [1], [2], [3],
       [0], [1], [2], [3], [0], [1], [2], [3]]
y =  [ [0], [0], [0], [0], [1], [1], [1], [1],
       [2], [2], [2], [2], [3], [3], [3], [3]]
z =  [ [0] for ir in range(0, rays) ]
px = [ [0] for ir in range(0, rays) ]
py = [ [0] for ir in range(0, rays) ]
pz = [ [1/v(x[ir][0], y[ir][0], 0)] for ir in range(0, rays) ]
# Initial conditions in ray-centered coordinates basis vectors
# e1 must be perpendicular to p, we choose (py, -px, 0) normalized
e11 =  [ [1] for ir in range(0, rays) ]
e12 =  [ [0] for ir in range(0, rays) ]
e13 =  [ [0] for ir in range(0, rays) ]

# p must be normalized for this to be correct
e31 =  [ [0] for ir in range(0, rays) ]
e32 =  [ [0] for ir in range(0, rays) ]
e33 =  [ [1] for ir in range(0, rays) ]

# e2 = e3 x e1
e21 = [ [e32[r][0]*e13[r][0] - e33[r][0]*e12[r][0] ] for r in range(0, rays) ]
e22 = [ [e33[r][0]*e11[r][0] - e31[r][0]*e13[r][0] ] for r in range(0, rays) ]
e23 = [ [e31[r][0]*e12[r][0] - e32[r][0]*e11[r][0] ] for r in range(0, rays) ]

# Π_0 = I
P11 = [ [1] for ir in range(0, rays) ]
P12 = [ [0] for ir in range(0, rays) ]
P13 = [ [0] for ir in range(0, rays) ]
P14 = [ [0] for ir in range(0, rays) ]
P21 = [ [0] for ir in range(0, rays) ]
P22 = [ [1] for ir in range(0, rays) ]
P23 = [ [0] for ir in range(0, rays) ]
P24 = [ [0] for ir in range(0, rays) ]
P31 = [ [0] for ir in range(0, rays) ]
P32 = [ [0] for ir in range(0, rays) ]
P33 = [ [1] for ir in range(0, rays) ]
P34 = [ [0] for ir in range(0, rays) ]
P41 = [ [0] for ir in range(0, rays) ]
P42 = [ [0] for ir in range(0, rays) ]
P43 = [ [0] for ir in range(0, rays) ]
P44 = [ [1] for ir in range(0, rays) ]

for n in range(0, int(round(N))):
    for r in range(0, rays):
        args = (t[n], x[r][n],  y[r][n], z[r][n], px[r][n], py[r][n], pz[r][n])

        # Estimate next X
        x[r].append(rk4(fx, h, 1, *args))
        y[r].append(rk4(fy, h, 2, *args))
        z[r].append(rk4(fz, h, 3, *args))

        # Estimate next P
        px[r].append(rk4(fpx, h, 4, *args))
        py[r].append(rk4(fpy, h, 5, *args))
        pz[r].append(rk4(fpz, h, 6, *args))

        args = (t[n],  x[r][n],   y[r][n],  z[r][n],
                      px[r][n],  py[r][n], pz[r][n],
                     e11[r][n], e12[r][n], e13[r][n])

        # Estimate next e3
        normp = sqrt( px[r][n+1]**2 + py[r][n+1]**2 + pz[r][n+1]**2 )
        e31[r].append(px[r][n+1]/normp)
        e32[r].append(py[r][n+1]/normp)
        e33[r].append(pz[r][n+1]/normp)

        # Estimate next e1
        E11 = rk4(fe11, h, 7, *args)
        E12 = rk4(fe12, h, 8, *args)
        E13 = rk4(fe13, h, 9, *args)
        # Make sure they are orthogonal, by removing projection on e3
        E1e3 = E11*e31[r][n+1] + E12*e32[r][n+1] + E13*e33[r][n+1]
        e11[r].append(E11 - E1e3*e31[r][n+1])
        e12[r].append(E12 - E1e3*e32[r][n+1])
        e13[r].append(E13 - E1e3*e33[r][n+1])

        # Estimate e2
        E21 = e32[r][n+1]*e13[r][n+1] - e33[r][n+1]*e12[r][n+1]
        E22 = e33[r][n+1]*e11[r][n+1] - e31[r][n+1]*e13[r][n+1]
        E23 = e31[r][n+1]*e12[r][n+1] - e32[r][n+1]*e11[r][n+1]
        normE2 = sqrt(E21**2 + E22**2 + E23**2)
        e21[r].append(E21/normE2)
        e22[r].append(E22/normE2)
        e23[r].append(E23/normE2)

        args = (t[n],  x[r][n],   y[r][n],  z[r][n],
                      px[r][n],  py[r][n], pz[r][n],
                     e11[r][n], e12[r][n], e13[r][n],
                     e21[r][n], e22[r][n], e23[r][n],
                     e31[r][n], e32[r][n], e33[r][n],
                     P11[r][n], P12[r][n], P13[r][n], P14[r][n],
                     P21[r][n], P22[r][n], P23[r][n], P24[r][n],
                     P31[r][n], P32[r][n], P33[r][n], P34[r][n],
                     P41[r][n], P42[r][n], P43[r][n], P44[r][n])
        # Estimate next Π
        P11[r].append(rk4(fP11, h, 16, *args))
        P12[r].append(rk4(fP12, h, 17, *args))
        P13[r].append(rk4(fP13, h, 18, *args))
        P14[r].append(rk4(fP14, h, 19, *args))
        P21[r].append(rk4(fP21, h, 20, *args))
        P22[r].append(rk4(fP22, h, 21, *args))
        P23[r].append(rk4(fP23, h, 22, *args))
        P24[r].append(rk4(fP24, h, 23, *args))
        P31[r].append(rk4(fP31, h, 24, *args))
        P32[r].append(rk4(fP32, h, 25, *args))
        P33[r].append(rk4(fP33, h, 26, *args))
        P34[r].append(rk4(fP34, h, 27, *args))
        P41[r].append(rk4(fP41, h, 28, *args))
        P42[r].append(rk4(fP42, h, 29, *args))
        P43[r].append(rk4(fP43, h, 30, *args))
        P44[r].append(rk4(fP44, h, 31, *args))

    t.append(t[n]+h)

# Plots
ir = 8
n = 0

# 2D
fig = plt.figure()
fig.suptitle(r"$(x, y, z)$ coordinates", fontsize=14)
ax1 = fig.add_subplot(131, title=ur"$x$–$z$ projection")
for r in range(0, rays):
    ax1.plot(x[r], z[r], 'k')
ax1.arrow(  x[ir][n],   z[ir][n],
          e11[ir][n], e13[ir][n],
          head_width=0.05, head_length=0.1, fc='r', ec='r')
ax1.arrow(  x[ir][n],   z[ir][n],
          e21[ir][n], e23[ir][n],
          head_width=0.05, head_length=0.1, fc='b', ec='b')
ax1.arrow(  x[ir][n], z[ir][n],
          e31[ir][n], e33[ir][n],
          head_width=0.05, head_length=0.1, fc='g', ec='g')
plt.gca().invert_yaxis()

ax2 = fig.add_subplot(132, title=ur"$x$–$y$ projection")
for r in range(0, rays):
    ax2.plot(x[r], y[r], 'k')
ax2.arrow(  x[ir][n],   y[ir][n],
          e11[ir][n], e12[ir][n],
          head_width=0.05, head_length=0.1, fc='r', ec='r')
ax2.arrow(  x[ir][n],   y[ir][n],
          e21[ir][n], e22[ir][n],
          head_width=0.05, head_length=0.1, fc='b', ec='b')
ax2.arrow(  x[ir][n],   y[ir][n],
          e31[ir][n], e32[ir][n],
          head_width=0.05, head_length=0.1, fc='g', ec='g')
plt.gca().invert_yaxis()

ax3 = fig.add_subplot(133, title=ur"$y$–$z$ projection")
for r in range(0, rays):
    ax3.plot(y[r], z[r], 'k')
ax3.arrow(  y[ir][n],   z[ir][n],
          e12[ir][n], e13[ir][n],
          head_width=0.05, head_length=0.1, fc='r', ec='r')
ax3.arrow(  y[ir][n],   z[ir][n],
          e22[ir][n], e23[ir][n],
          head_width=0.05, head_length=0.1, fc='b', ec='b')
ax3.arrow(  y[ir][n],   z[ir][n],
          e32[ir][n], e33[ir][n],
          head_width=0.05, head_length=0.1, fc='g', ec='g')
plt.gca().invert_yaxis()

plt.show()

# 3D
fig = plt.figure()
ax = fig.gca(projection='3d', title="Ray Tracing")
ax.set_aspect("equal")
ax.set_xlabel(r"$x$", fontsize=16)
ax.set_ylabel(r"$y$", fontsize=16)
ax.set_zlabel(r"$z$", fontsize=16)
for r in range(0, rays):
    ax.plot(x[r], y[r], z[r])
#a1 = Arrow3D([x[ir][n], e11[ir][n]],
             #[y[ir][n], e12[ir][n]],
             #[z[ir][n], e13[ir][n]],
             #mutation_scale=20, lw=1, arrowstyle="-|>", color="r")
#a2 = Arrow3D([x[ir][n], e21[ir][n]],
             #[y[ir][n], e22[ir][n]],
             #[z[ir][n], e23[ir][n]],
             #mutation_scale=20, lw=1, arrowstyle="-|>", color="b")
#a3 = Arrow3D([x[ir][n], e31[ir][n]],
             #[y[ir][n], e32[ir][n]],
             #[z[ir][n], e33[ir][n]],
             #mutation_scale=20, lw=1, arrowstyle="-|>", color="g")
#ax.add_artist(a1)
#ax.add_artist(a2)
#ax.add_artist(a3)
ax.set_zlim(ax.get_zlim3d()[::-1])
plt.show()

#fig = plt.figure()
#ax = fig.gca(projection='3d', title="Ray Tracing")
#ax.set_xlabel(r"$p_x$", fontsize=16)
#ax.set_ylabel(r"$p_y$", fontsize=16)
#ax.set_zlabel(r"$p_z$", fontsize=16)
#for r in range(0, rays):
    #ax.plot(px[r], py[r], pz[r])
#ax.set_zlim(ax.get_zlim3d()[::-1])
#plt.show()

#fig = plt.figure()
#ax = fig.gca(projection='3d', title="Ray Tracing")
#ax.set_xlabel(r"$e_{11}$", fontsize=16)
#ax.set_ylabel(r"$e_{12}$", fontsize=16)
#ax.set_zlabel(r"$e_{13}$", fontsize=16)
#for r in range(0, rays):
    #ax.plot(e11[r], e12[r], e13[r])
#ax.set_zlim(ax.get_zlim3d()[::-1])
#plt.show()

#fig = plt.figure()
#ax = fig.gca(projection='3d', title="Ray Tracing")
#ax.set_xlabel(r"$e_{21}$", fontsize=16)
#ax.set_ylabel(r"$e_{22}$", fontsize=16)
#ax.set_zlabel(r"$e_{23}$", fontsize=16)
#for r in range(0, rays):
    #ax.plot(e21[r], e22[r], e23[r])
#ax.set_zlim(ax.get_zlim3d()[::-1])
#plt.show()

#fig = plt.figure()
#ax = fig.gca(projection='3d', title="Ray Tracing")
#ax.set_xlabel(r"$e_{31}$", fontsize=16)
#ax.set_ylabel(r"$e_{32}$", fontsize=16)
#ax.set_zlabel(r"$e_{33}$", fontsize=16)
#for r in range(0, rays):
    #ax.plot(e31[r], e32[r], e33[r])
#ax.set_zlim(ax.get_zlim3d()[::-1])
#plt.show()
